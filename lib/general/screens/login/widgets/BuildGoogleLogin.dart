part of 'LoginWidgetsImports.dart';

class BuildGoogleLogin extends StatelessWidget {
  const BuildGoogleLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: ()=> GoogleSignIn().signIn(),
        child: Container(
          width: 180,
          margin: const EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                color: MyColors.greyWhite,
                spreadRadius: 3,
                blurRadius: 5
              )
            ]
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CachedImage(
                height: 30,
                width: 30,
                url:
                    "https://www.freepnglogos.com/uploads/google-logo-png/google-logo-png-google-icon-logo-png-transparent-svg-vector-bie-supply-14.png",
              ),
              SizedBox(width: 10),
              MyText(
                title: tr(context,'googleLogin'),
                color: MyColors.black,
                size: 12,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
